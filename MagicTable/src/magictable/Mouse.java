package magictable;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Mouse extends JPanel implements MouseListener, MouseMotionListener {
    private int index = 0;
    private Point[] arr = new Point[100000];

    public Mouse(String name) {
        super();
        index = 0;
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        JFrame fr = new JFrame(name);
        fr.add(this);
        fr.setSize(500, 500);
        setBackground(Color.green);
        fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fr.setVisible(true);

    }
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponents(g);
        //g.drawString("TESTE", 10, 10);
        for (int i = 0; i < index - 1; i++)
            g.drawLine(arr[i].x, arr[i].y, arr[i + 1].x, arr[i + 1].y);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        arr[index] = new Point(e.getX(), e.getY());
      
         System.out.println("Enviar dragged: " + arr[index].getX() + " " + arr[index].getY());
           index++;
        repaint();
    }
    @Override
    public void mousePressed(MouseEvent e) {
        arr[index] = new Point(e.getX(), e.getY());
         System.out.println("Enviar pressed: " + arr[index].getX() + " " + arr[index].getY());
        index++;
       
        repaint();
    }

    @Override
    public void mouseExited(MouseEvent e) {}
    @Override
    public void mouseClicked(MouseEvent e) {}
    @Override
    public void mouseEntered(MouseEvent e) {}
    @Override
    public void mouseReleased(MouseEvent e) {
    
        arr = new Point[100000];
        index = 0;
      
    }
    public void mouseMoved(MouseEvent e) {}

    public static void main(String[] args) {
        Mouse mouse = new Mouse("Mouse");
    }
}
